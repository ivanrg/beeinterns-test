export default class Lection {
    constructor(templateSelector, {link, title, subTitle, date, category}) {
        this._lectionElement = this._getLectionElement(templateSelector);
        this._lectionCard = this._lectionElement.querySelector('.lection-card');
        this._lectionThumbnail = this._lectionElement.querySelector('.lection-card__thumbnail');
        this._lectionTitle = this._lectionElement.querySelector('.lection-card__title');
        this._lectionSubtitle = this._lectionElement.querySelector('.lection-card__desc');
        this._lectionDate = this._lectionElement.querySelector('.lection-card__date');

        this._link = link;
        this._title = title;
        this._subTitle = subTitle;
        this._date = date;
        this._category = category;
    }

    _getLectionElement(templateSelector) {
        return document.querySelector(templateSelector)
            .content
            .cloneNode(true)
    }

    generateLection() {
        this._lectionThumbnail.src = this._link;
        this._lectionTitle.textContent = this._title;
        this._lectionSubtitle.textContent = this._subTitle;
        this._lectionDate.textContent = this._date;
        this._lectionCard.dataset.group = this._category;
        return this._lectionElement;
    }
}