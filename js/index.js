import Section from "./Section.js";
import Lection from "./Lection.js";
import {lectionsList} from "../data/LectionsList.js";

const baseLevelBlock = document.querySelector('.content-block')
const blockTitles = baseLevelBlock.querySelectorAll('.lection-card__title');
const blockDescs = baseLevelBlock.querySelectorAll('.lection-card__desc');
const leftMenuItemLink = document.querySelectorAll('.leftbar__item-link');
const leftBarMenuContainer = document.querySelector('.leftbar__item-menu-container');

const controllButtons = document.querySelectorAll('.btn_type_control')

const capitalizeText = (element) => {
    if (element) {
        element.textContent = element.textContent.toUpperCase()
    }
}

const fitText = (element) => {
    if (element && element.textContent.length > 20) {
        element.textContent = `${element.textContent.slice(0, 20)}...`
    }
}

blockTitles.forEach(titleElement => {
    capitalizeText(titleElement);
})
blockDescs.forEach((descElement) => {
    fitText(descElement)
})

const handleSideMenu = () => {
    leftMenuItemLink.forEach((link) => {
        link.addEventListener('click', (e) => {
            link.querySelector('.leftbar__item-arrow').classList.toggle('leftbar__item-arrow_closed');
            leftBarMenuContainer.classList.toggle('leftbar__item-menu-container_hidden');
        })
    })
}

const createLectionCard = (templateSelector, link, title, subTitle, date, category) => {
    const lectionCard = new Lection(
        templateSelector,
        {link, title, subTitle, date, category})
    return lectionCard.generateLection()
}

const allLectionsSection = new Section({
    renderer: (item) => {
        const lectionCard = createLectionCard(
            '#card-template',
            item.link,
            item.title,
            item.subTitle,
            item.date,
            item.category
        );
        allLectionsSection.addItem(lectionCard);
    },
    containerSelector: '.content-block_all-lections'
})

const renderLectionCards = (lections) => {
  if(lections) {
      const initialCards = [];
      lections.forEach((item) => {
          initialCards.push({link: item.link, title: item.title, subTitle:item.subTitle, date: item.date, category: item.category})
      })
      allLectionsSection.renderItems(initialCards)
  }  else {
      console.log('Cards list is empty');
  }
}

const applyFilter = (category) => {
    if(category === 'all-lections') {
        return renderLectionCards(lectionsList)
    }
    const filteredList = lectionsList.filter((lection) => {
        return lection.category === category
    });
    renderLectionCards(filteredList);
}

const grabLectionsInfo = () => {
    const lectionCards = document.querySelectorAll('.content-block_all-lections .lection-card');
    const res = {};
    if(lectionCards) {
        lectionCards.forEach((item) => {
            const newLection = {
                "title": item.querySelector('.lection-card__title').textContent,
                "description": item.querySelector('.lection-card__desc').textContent,
                "date": item.querySelector('.lection-card__date').textContent,
                "image": item.querySelector('.lection-card__thumbnail').src,
                "label": "лекция"
            }
            let lections = res[item.dataset.group];
            if(!lections) {
                lections = [];
            }
            lections.push(newLection);
            res[item.dataset.group] = prevArr;
        })
    }
    return res
}

controllButtons.forEach((btn) => {
    btn.addEventListener('click', (e) => {
        applyFilter(e.target.id);
    })
})

handleSideMenu();
renderLectionCards(lectionsList);
console.log(grabLectionsInfo())
