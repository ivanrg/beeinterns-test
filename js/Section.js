export default class Section {
    constructor({renderer, containerSelector}) {
        this._renderer = renderer;
        this._container = document.querySelector(containerSelector);
        this._lectionsContainer = this._container.querySelector('.lection-cards');
        this._itemsCounter = this._container.querySelector('#all-lections');
    }

    renderItems(items) {
        this.clear();
        this._setItemsNumber(items.length)
        items.forEach(item => this._renderer(item))
    }

    addItem(element) {
        this._container.querySelector('.lection-cards').prepend(element);
    }

    _setItemsNumber(n) {
        let lectionsWordCorrectForm;
        lectionsWordCorrectForm = n === 1
            ? 'лекция'
            : (n === 2 || n === 3 || n === 3 || n === 4)
            ? 'лекции'
            : 'лекций'
        this._itemsCounter.textContent = `${n} ${lectionsWordCorrectForm}`;
    }

    clear() {
        this._lectionsContainer.innerHTML = ''
    }
}